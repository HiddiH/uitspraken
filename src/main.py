from . import app
from flask import render_template, request
import sqlite3
from datetime import datetime

@app.route('/', methods=["GET", "POST"])
def index():
    conn = sqlite3.connect("uitspraken.db")
    c = conn.cursor()
    if request.method == "POST" :
        data = request.form
        c.execute("INSERT INTO uitspraken (uitspraak, user, datum) VALUES (?, ?, ?)", (data["quote"], data["user"], datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        conn.commit()
    c.execute("SELECT * FROM users")
    users = c.fetchall()
    c.execute("SELECT q.datum, q.uitspraak, u.name FROM uitspraken q left join users u on q.user=u.id")
    quotes = c.fetchall()
    return render_template("weNeedCSS.html", users = [{"name": u[1], "id": u[0]} for u in users], quotes = [{"user": q[2], "text": q[1], "date": q[0]} for q in quotes])
